package spcam;

import java.util.*;
import java.io.*;

public class Configuration {

    private static final String CONFIGURATION_FILE = "/config.properties";

    private static final Properties properties;

    // загрузка конфига при инициализации класса
    static {
        properties = new Properties();
        try (InputStream inputStream = Configuration.class.getResourceAsStream(CONFIGURATION_FILE)) {
            properties.load(inputStream);
        } catch (IOException e) {
            throw new RuntimeException("Failed to read file " + CONFIGURATION_FILE, e);
        }
    }

    public static Map<String, String> getConfiguration() {
        Map temp = properties;
        Map<String, String> map = new HashMap<String, String>(temp);

        return Collections.unmodifiableMap(map);
    }


    public static String getConfigurationValue(String key) {
        return properties.getProperty(key);
    }

    private Configuration() {
    }

}