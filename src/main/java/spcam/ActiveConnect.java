package spcam;

import org.springframework.stereotype.Component;
import org.apache.activemq.ActiveMQConnectionFactory;
import javax.jms.ConnectionFactory;


@Component
public class ActiveConnect {

    private String url = Configuration.getConfigurationValue("mq.host");

    public ConnectionFactory newConnect () {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(this.url);
        return connectionFactory;
    }
}
