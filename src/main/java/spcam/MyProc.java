package spcam;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;


@Component
public class MyProc implements Processor{

    private MyIterator myIterator = new MyIterator();
    //public static AtomicLong counter = new AtomicLong();

    @Override
    public void process(Exchange exchange) throws Exception {
        // вытащим мапу
//        ArrayList<HashMap<String, String>> dt= (ArrayList<HashMap<String, String>>) exchange.getIn().getBody();

        List<Map<String, String>> exchBodyList = (List<Map<String, String>>) exchange.getIn().getBody();

        // Создадим экземпляр класса для дальнейшей json-изации
        JsonClass jsonClass = new JsonClass(exchBodyList.get(0).get("UniqueID"),
                exchBodyList.get(0).get("ProductCode"),
                exchBodyList.get(0).get("ProductName"),
                Double.parseDouble(exchBodyList.get(0).get("PriceWholesale")),
                Double.parseDouble(exchBodyList.get(0).get("PriceRetail")),
                Integer.parseInt(exchBodyList.get(0).get("InStock"))
        );

        // Соберём json при помощи Jackson
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(jsonClass);
        //System.out.println("JSON: " + json);

        // Счётчик обработанных строк
        myIterator.inc();
        // альтернативный итератор
        exchange.setProperty("Counter", myIterator.getI());
        exchange.getIn().setBody(json);
    }
}

