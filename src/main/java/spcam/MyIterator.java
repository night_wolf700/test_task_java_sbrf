package spcam;

import org.springframework.stereotype.Component;

@Component
public class MyIterator {

    private int i = 0;

    public synchronized int getI() {
        return i;
    }

    public synchronized void inc() {
        this.i++;
        System.out.println("Строк обработано: " + this.i);
    }
}
