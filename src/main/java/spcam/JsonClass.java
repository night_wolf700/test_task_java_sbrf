package spcam;

public class JsonClass{

    public String UniqueID;
    public String ProductCode;
    public String ProductName;
    public double PriceWholesale;
    public double PriceRetail;
    public int InStock;

    JsonClass (String UniqueID, String ProductCode, String ProductName, double PriceWholesale, double PriceRetail, int InStock) {
        this.UniqueID = UniqueID;
        this.ProductCode = ProductCode;
        this.ProductName = ProductName;
        this.PriceWholesale = PriceWholesale;
        this.PriceRetail = PriceRetail;
        this.InStock = InStock;
    }


}
