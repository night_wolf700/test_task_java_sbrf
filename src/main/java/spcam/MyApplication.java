package spcam;

import org.apache.camel.CamelContext;
import org.apache.camel.impl.DefaultCamelContext;
import javax.jms.ConnectionFactory;
import org.apache.camel.component.jms.JmsComponent;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class MyApplication {

    public static void main(String[] args) throws Exception {

        SpringApplication.run(MyApplication.class, args);

        FileMoveRoute fileMoveRoute = new FileMoveRoute();
        CamelContext context = new DefaultCamelContext();

        ConnectionFactory connect = new ActiveConnect().newConnect();
        context.addComponent("jms",
                JmsComponent.jmsComponentAutoAcknowledge(connect));

        context.addRoutes(fileMoveRoute);
        try {
            context.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}