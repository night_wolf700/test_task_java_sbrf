package spcam;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.CsvDataFormat;
import org.springframework.stereotype.Component;
import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


@Component
public class FileMoveRoute extends RouteBuilder {

    private MyProc proc = new MyProc();
    private String path = Configuration.getConfigurationValue("file.path");

    @Override
    public void configure() {
        CsvDataFormat csv = new CsvDataFormat();
        csv.setDelimiter(";");
        csv.setUseMaps(true);
        csv.setHeader(Arrays.asList("UniqueID","ProductCode","ProductName","PriceWholesale","PriceRetail","InStock"));
        ExecutorService executor = Executors.newFixedThreadPool(
                Integer.parseInt(Configuration.getConfigurationValue("camel.thread.count")));

        from(path + "?noop=true") // не удалять исходные файлы
                .routeId("csvToJsonRoute")
                .choice()
                .when(header("CamelFileName").endsWith(".csv"))
                .split(body().tokenize("\n")) // split - потокова передача. Грузим по строкам.
                .streaming().parallelProcessing().executorService(executor) // многопоточная загрузка
                .unmarshal(csv)  // получаем List<Map>
                .process(proc)   // процессим, собираем Json
                //.marshal().json(JsonLibrary.Jackson) // альтернативный варианнт собрать JSON. Не очень удобно для конфигурации
                .to("jms:JsonQueue")
                //.log("Строк обработано: " + "${exchangeProperty.Counter}")
                //.to("seda:my")
                .end();
    //from("seda:my")
    }

}