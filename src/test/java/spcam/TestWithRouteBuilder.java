package spcam;

import org.apache.camel.builder.AdviceWithRouteBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.junit.Before;
import org.junit.Test;


public class TestWithRouteBuilder extends CamelTestSupport {

    @Override
    protected RouteBuilder createRouteBuilder(){
        return new FileMoveRoute();
    }

    @Override
    public boolean isUseAdviceWith() {
        return true;
    }

    @Before
    public void mockEndpoints() throws Exception{
        AdviceWithRouteBuilder mockJms = new AdviceWithRouteBuilder(){

            private String path = Configuration.getConfigurationValue("file.path.test");

            @Override
            public void configure() throws Exception{
                replaceFromWith(path + "?noop=true"); // подемняем входной каталог
                interceptSendToEndpoint("jms:JsonQueue") // перехват конечной точки маршрута
                .skipSendToOriginalEndpoint() // не отправляем сообщение в исходную точку маршрута
                        .to("mock:TestQueue");
            }
        };
        context.getRouteDefinition("csvToJsonRoute")
                .adviceWith(context, mockJms);
    }

    @Test
    public void testRoute() throws Exception{
        MockEndpoint mock = getMockEndpoint("mock:TestQueue");

        context.start();
        // проверим кол-во переданных сообщений
        mock.expectedMessageCount(3);
        mock.setAssertPeriod(500);
        assertMockEndpointsSatisfied();

        // сравним строку с эталоном
        String line1 = mock.getReceivedExchanges().get(0).getIn().getBody(String.class);
        System.out.println(line1);
        String etalon="{\"UniqueID\":\"5ab067d0-dbac-4abd-9413-edabf953edf6\",\"ProductCode\":\"04443-22491\"," +
                "\"ProductName\":\"BOOSTER KIT\",\"PriceWholesale\":112.5,\"PriceRetail\":126.85,\"InStock\":218}";
        System.out.println(etalon);
        assertEquals(etalon, mock.getReceivedExchanges().get(0).getIn().getBody(String.class));
        context.stop();
    }

}
