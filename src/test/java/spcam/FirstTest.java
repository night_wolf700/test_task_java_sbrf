package spcam;

import javax.jms.ConnectionFactory;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.model.dataformat.CsvDataFormat;
import org.apache.camel.test.junit4.CamelTestSupport;
import org.apache.camel.component.mock.MockEndpoint;
import org.junit.Test;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class FirstTest extends CamelTestSupport {
    @Override
    protected CamelContext createCamelContext() throws Exception {
        // create CamelContext
        CamelContext context = super.createCamelContext();

        // connect to embedded ActiveMQ JMS broker
        ConnectionFactory connect = new ActiveConnect().newConnect();
        context.addComponent("jms",
                JmsComponent.jmsComponentAutoAcknowledge(connect));

        return context;
    }

    @Test
    public void testPlacingOrders() throws Exception {
        MockEndpoint mock = getMockEndpoint("mock:TestQueue");

        // проверим кол-во переданных сообщений
        mock.expectedMessageCount(3);
        mock.setAssertPeriod(500);
        assertMockEndpointsSatisfied();

        // сравним строку с эталоном
        String line1 = mock.getReceivedExchanges().get(0).getIn().getBody(String.class);
        System.out.println(line1);
        String etalon="{\"UniqueID\":\"5ab067d0-dbac-4abd-9413-edabf953edf6\",\"ProductCode\":\"04443-22491\"," +
                "\"ProductName\":\"BOOSTER KIT\",\"PriceWholesale\":112.5,\"PriceRetail\":126.85,\"InStock\":218}";
        System.out.println(etalon);
        assertEquals(etalon, mock.getReceivedExchanges().get(0).getIn().getBody(String.class));


    }

    @Override
    protected RouteBuilder createRouteBuilder() throws Exception {
        return new RouteBuilder() {

            private MyProc proc = new MyProc();
            private String path = Configuration.getConfigurationValue("file.path.test");

            @Override
            public void configure() throws Exception {
                context.setTracing(true);
                CsvDataFormat csv = new CsvDataFormat();
                csv.setDelimiter(";");
                csv.setUseMaps(true);
                csv.setHeader(Arrays.asList("UniqueID","ProductCode","ProductName","PriceWholesale","PriceRetail","InStock"));
                ExecutorService executor = Executors.newFixedThreadPool(
                        Integer.parseInt(Configuration.getConfigurationValue("camel.thread.count")));
                from(path + "?noop=true") // не удалять исходные файлы
                        .choice()
                        .when(header("CamelFileName").endsWith(".csv"))
                        .split(body().tokenize("\n")) // split - потокова передача. Грузим по строкам.
                        .streaming().parallelProcessing().executorService(executor) // многопоточная загрузка
                        .unmarshal(csv)  // получаем List<Map>
                        .process(proc)   // процессим, собираем Json
                        .to("jms:TestQueue")
                        .end();

                from("jms:TestQueue")
                        .to("mock:TestQueue");
            }
        };
    }
}
